package spec;
import org.testng.annotations.Test;
import pageobjects.GoldFishPage;

import java.io.FileNotFoundException;

public class GoldFishSpec extends GoldFishPage {

    @Test
    public static void goToPageURL () { hitAGoldFishURL(); } // To verify Page URL to read quotes from and save into a file.

    @Test (priority = 1)
    public static void checkPageTitle () {
        verifyTitle();
    } // To verify page title

    @Test (priority = 2)
    public static void getAllQuotesByAnAuthor () throws FileNotFoundException, InterruptedException { allQuotesByGideon(); } // To save all quotes of a particular author in a file
}
