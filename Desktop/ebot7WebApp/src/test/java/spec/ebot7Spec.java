package spec;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.ebot7Page;

import java.io.IOException;

class ebot7Spec extends ebot7Page {
    @Test(priority = 3)
    public static void goToPageURL () { hitebot7WebAppURL(); } // To verify Page URL to read quotes from and save into a file.

    @Test (priority = 4)
    public static void verifyPageTitle () throws InterruptedException { verifyTitle();} // To verify page title.

    @Test (priority = 6) // To insert all quotes from a file to web app
    public static void insertAQuote () throws IOException, InterruptedException {
        insertQuotesByAnAuthor();
    }

    // Below 2 test cases are meant to fail as they are bugs.

    @Test (priority = 7) //Web app accepts a quote with character length > 200.
    public static void addNewQuoteScenario () throws InterruptedException {
        fieldValidationBug();
    }

    @Test (priority = 8) //Searched Quote is not filtered in results in selected state.
    public static void validateSearchScenario() throws InterruptedException {
        Assert.assertTrue(searchResultsNotUpdated(),   "validateSearchScenario(): FAILED");
    }
}
