package pageobjects;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;
import java.util.Date;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ebot7Page {

    // Declare Variables
    private final static String TAG = "    ebot7page():                 | "; // For logging
    private static final Logger logger = LogManager.getLogger(GoldFishPage.class); // Logger class object to utilize log levels.
    private static WebDriver driver; // driver
    private static Date today = new Date(); // Just to get a unique string
    private final static String ebot7WebAppURL =  "https://www-5ebb98ff524ed83c898fedc3.recruit.eb7.io/"; // Desired page URL
    private static int lineCount = 0; // To count the number of lines in a file.
    private static File file = new File("GideonQuotes.txt"); // File where quotes would be read from.

    // Functions

    // set up driver
    @BeforeTest
    public static void beforeTest()
    {
        logger.info(TAG + "beforeTest()");
        WebDriverManager.chromedriver().setup(); // Set up chrome driver
        ChromeOptions options = new ChromeOptions(); // Add chrome options
        options.addArguments("enable-automation");
        options.addArguments("--no-sandbox");
        //options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        driver = new ChromeDriver(options); // Make driver include chrome options.
    }

    // Get URL
    protected static void hitebot7WebAppURL() {
        logger.info(TAG + "hitebot7WebAppURL()");
        driver.get(ebot7WebAppURL); // Hit AGoldFishURL
    }

    // Check page title.
    protected static void verifyTitle() throws InterruptedException {
        logger.info(TAG + "verifyTitle()");
        String pageTitle = "e-bot7 - Sandbox";
        Assert.assertEquals(driver.getTitle(), pageTitle, TAG + " verifyTitle(): FAILED");
        Thread.sleep(3000);
    }

    // Get the line count of the file
    private static int getLineCount() throws IOException {
        logger.info(TAG + "getLineCount()");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        while (reader.readLine() != null) {lineCount++;}
        reader.close();
        return lineCount;
    }

    // Read any line from the Quotes file GideonQuotes.txt
    private static String readNthLine(int line) throws IOException {
        logger.info(TAG + "readNthLine()");
        String[] quotePart = new String[0];
        if (line <= getLineCount()) {
            try {
                String completeLine = Files.lines(Paths.get("GideonQuotes.txt")).skip(line - 1).findFirst().get();
                quotePart = completeLine.split(":");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return quotePart[1];
    }

    // Click Save button on Add new quote modal.
    private static void clickSaveButton(){
        logger.info(TAG + "clickSaveButton()");
        WebElement saveButton = driver.findElement(By.cssSelector("button[class='btn btn-success modal-default-button']"));
        saveButton.click();
    }

    // Click Add new quote button
    private static void clickAddNewQuoteButton() {
        logger.info(TAG + "clickAddNewQuoteButton()");
        WebElement addNewQuote = driver.findElement(By.id("show-modal"));
        addNewQuote.click();
    }

    // Enter Author name in author text field.
    private static void insertAuthorName(String author) {
        logger.info(TAG + "insertAuthorName()");
        WebElement authorField = driver.findElement(By.id("autorInput"));
        authorField.click();
        authorField.sendKeys(author);
    }

    // Enter quote in quote text field.
    private static void insertQuote(String quote) {
        logger.info(TAG + "insertQuote()");
        WebElement quoteField = driver.findElement(By.id("quoteInput"));
        quoteField.click();
        quoteField.sendKeys(quote);
    }


    // Click Search field
    private static void clickSearchField(){
        logger.info(TAG + "clickSearchField()");
        WebElement searchField = driver.findElement(By.id("searchBar"));
        searchField.click();
    }

    // Get Search field to manipulate
    private static WebElement getSearchField() {
        logger.info(TAG + "getSearchField()");
        return driver.findElement(By.id("searchBar"));
    }

    // Set Search field
    private static void setSearchField(String uniqueQuote) {
        logger.info(TAG + "setSearchField()");
        getSearchField().sendKeys(uniqueQuote);
    }

    // Check if max character length validation is applied on added quote.
    private static void searchMaxCharValidation() throws InterruptedException {
        Thread.sleep(10000);
        int length;
        String actString = driver.findElement(By.xpath("/html/body/div/div/div/div[2]/div[2]/section/ul/li[2]/p")).getText();
        length = actString.length();
        Assert.assertEquals(length, 200, TAG + "fieldValidationBug: FAILED");
        getSearchField().clear();
    }

    // Insert all quotes of a particular author.
    protected static void insertQuotesByAnAuthor() throws IOException, InterruptedException {
        logger.info(TAG + "insertQuotesByAnAuthor()");
        String quote = " ";
        int lineCount = getLineCount();
        for (int i =1; i <= lineCount; i++){
            logger.info(TAG + "insertQuotesByAnAuthor(): Entering line number:" + i);
            clickAddNewQuoteButton();
            insertAuthorName("Gideon");
            quote = readNthLine(i);
            insertQuote(quote);
            clickSaveButton();
            Thread.sleep(2000);
            if (i==1) {
                driver.findElement(By.xpath("//li[contains(text(),'Gideon')]")).click();
            }
            if (i == lineCount) {
                String actStr = driver.findElement(By.cssSelector("li[class='quotes__header']")).getText();
                Assert.assertTrue(actStr.contains(String.valueOf(lineCount)), TAG + "insertQuotesByAnAuthor(): FAILED");
            }
        }
    }

    // Bug: Check field validation for new quote.
    protected static void fieldValidationBug() throws InterruptedException {
        logger.info(TAG + "fieldValidationBug()");
        String uniqueQuote = Long.toString(today.getTime());
        StringBuilder sb=new StringBuilder("");
        for (int i = 0; i<16; i++ ){sb.append(uniqueQuote);}
        clickAddNewQuoteButton();
        insertAuthorName("Zeeshan");
        insertQuote(sb.toString());
        clickSaveButton();
        clickSearchField();
        setSearchField(sb.toString());
        searchMaxCharValidation();
    }

    // Bug: Check if search results gets updated if user has selected an author who or whose quote is not in search.
    protected static boolean searchResultsNotUpdated() throws InterruptedException {
        logger.info(TAG + "searchResultsNotUpdated()");
        WebElement secondAuthor = driver.findElement(By.xpath("/html/body/div/div/div/div[2]/div[2]/nav/ul/li[2]"));
        secondAuthor.click();
        clickSearchField();
        setSearchField("What purpose light would serve if there is no darkness!!");
        Thread.sleep(5000);
        if (secondAuthor!=null)
            return false;
        else
            return true;
    }

    // Destroy driver
    @AfterTest
    public static void afterTest(){
        logger.info(TAG + "afterTest()");
        driver.close();
        driver.quit();
    }

}
