package pageobjects;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class GoldFishPage {

    // Declare Variables
    private final static String TAG = "    GoldFishPage():              | "; // For logging
    private static final Logger logger = LogManager.getLogger(GoldFishPage.class); // Logger class object to utilize log levels.
    private static WebDriver driver; // driver
    private final static String aGoldFishURL =  "https://agoldoffish.wordpress.com/criminal-minds-opening-and-closing-quotes/"; // Desired page URL

    // Functions

    // set up driver
    @BeforeTest
    public static void beforeTest()
    {
        logger.info(TAG + "beforeTest()");
        WebDriverManager.chromedriver().setup(); // Set up chrome driver
        ChromeOptions options = new ChromeOptions(); // Add chrome options
        options.addArguments("enable-automation");
        options.addArguments("--no-sandbox");
        //options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        driver = new ChromeDriver(options); // Make driver include chrome options.
    }

    // Get URL
    protected static void hitAGoldFishURL(){
        logger.info(TAG + "hitAGoldFishURL()");
        driver.get(aGoldFishURL); // Hit AGoldFishURL
    }

    // Check page title.
    protected static void verifyTitle(){
        logger.info(TAG + "verifyTitle()");
        String pageTitle = "Criminal Minds opening and closing quotes | Stewartry";
        Assert.assertEquals(driver.getTitle(), pageTitle, TAG + " verifyTitle(): FAILED");
    }

    // Read all quotes from Gideon and save them in a file.
    protected static void allQuotesByGideon() throws InterruptedException, FileNotFoundException {
        logger.info(TAG + "getAllQuotesByGideon()");
        WebElement quoteBody = driver.findElement(By.id("post-462"));
        List<WebElement> allQuotesByGideon = quoteBody.findElements(By.tagName("p"));
        String text = "";
        for (WebElement ele:allQuotesByGideon){
            if (ele.getText().contains("Gideon"))
                text = text + ele.getText() + "\n";
        }
            try {
            File newTextFile = new File("GideonQuotes.txt");

            FileWriter fw = new FileWriter(newTextFile);
            fw.write(text);
            fw.close();
            } catch (IOException exception) {
            exception.printStackTrace();
            }
    }

    // Destroy driver
    @AfterTest
    public static void afterTest(){
        logger.info(TAG + "afterTest()");
        driver.quit();
    }
}
