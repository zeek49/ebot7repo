How to run Tests

Pre-requisites
JDK should be installed and path should be set on which we’re planning to execute script
Maven should be installed and path should be set on which we’re planning to execute script
Clone project from the repository link given below.
In order to run test from Command Line -> Navigate to project directory and execute command “mvn clean test -Dsurefire.suiteXmlFiles=/Users/zeeshan.khalid/Desktop/ebot7WebApp/src/test/RunAll.xml”
For report generation from Command line -> install allure -> run command “allure serve”.


Project repository:
https://zeek49@bitbucket.org/zeek49/ebot7repo


* Each spec is separately executable.
